# Deploy a lighthouse instance for nebula on Openstack cloud
  
## Prerequisites

Configure your credentials for openstack in:
`~/.config/openstack/clouds.yaml`
using this example:

```console
clouds:
  cloud_name:
    auth:
      auth_url: https://auth.cloud.ovh.net/v3
      project_id: XXXXXXXX
      project_name: XXXXXXXX
      user_domain_name: Default
      username: XXXXXXX
    region_name: BHS5
    identity_api_version: '3'
```

```pip3 install -r requirements.txt --user```

## Run

```ansible-playbook all.yml -i inventory/hosts.ini```

## Usage

This ansible playbook only configures the lighthouse nodes for nebula.
If you need to setup your clients then follow the official documentation:
https://github.com/slackhq/nebula
